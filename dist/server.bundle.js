/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__dirname) {'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _express = __webpack_require__(1);

	var _express2 = _interopRequireDefault(_express);

	var _compression = __webpack_require__(2);

	var _compression2 = _interopRequireDefault(_compression);

	var _mongoose = __webpack_require__(3);

	var _mongoose2 = _interopRequireDefault(_mongoose);

	var _bodyParser = __webpack_require__(4);

	var _bodyParser2 = _interopRequireDefault(_bodyParser);

	var _cookieParser = __webpack_require__(5);

	var _cookieParser2 = _interopRequireDefault(_cookieParser);

	var _path = __webpack_require__(6);

	var _path2 = _interopRequireDefault(_path);

	var _webpack = __webpack_require__(7);

	var _webpack2 = _interopRequireDefault(_webpack);

	var _webpackDevMiddleware = __webpack_require__(8);

	var _webpackDevMiddleware2 = _interopRequireDefault(_webpackDevMiddleware);

	var _webpackHotMiddleware = __webpack_require__(9);

	var _webpackHotMiddleware2 = _interopRequireDefault(_webpackHotMiddleware);

	var _passport = __webpack_require__(10);

	var _passport2 = _interopRequireDefault(_passport);

	var _restaurant = __webpack_require__(11);

	var _restaurant2 = _interopRequireDefault(_restaurant);

	var _user = __webpack_require__(23);

	var _user2 = _interopRequireDefault(_user);

	var _config = __webpack_require__(25);

	var _config2 = _interopRequireDefault(_config);

	var _session = __webpack_require__(26);

	var _session2 = _interopRequireDefault(_session);

	var _restaurant3 = __webpack_require__(21);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	// Initialize the Express App


	// Webpack Requirements
	var app = new _express2.default();

	// Import required modules

	// import initPassport from './config/passport';


	// Set native promises as mongoose promise
	_mongoose2.default.Promise = global.Promise;

	app.use(_express2.default.static(_path2.default.resolve(__dirname, '../dist')));
	app.use(_bodyParser2.default.json({ limit: '20mb' }));
	app.use(_bodyParser2.default.urlencoded({ limit: '20mb', extended: true }));
	app.use((0, _compression2.default)());
	app.use((0, _cookieParser2.default)());
	(0, _session2.default)(app);
	app.use(_passport2.default.initialize());
	app.use(_passport2.default.session());

	// MongoDB Connection
	_mongoose2.default.connect(_config2.default.mongoURL, function (error) {
	  if (error) {
	    console.error('Please make sure Mongodb is installed and running!'); // eslint-disable-line no-console
	    throw error;
	  } else {
	    var db = _mongoose2.default.connection;
	    var initPassport = __webpack_require__(30);
	    initPassport(app, _passport2.default);
	    (0, _restaurant3.invokeRolesPolicies)();
	  }
	});

	app.use('/api', _restaurant2.default);
	app.use('/users', _user2.default);

	app.use(function (req, res, next) {
	  return res.status(200).send('Not found!');
	});

	// start app
	app.listen(_config2.default.port, function (error) {
	  if (!error) {
	    console.log('Daily Meal Server is running on: ' + _config2.default.port + '!'); // eslint-disable-line
	  }
	});

	exports.default = app;
	/* WEBPACK VAR INJECTION */}.call(exports, "server"))

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = require("express");

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = require("compression");

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = require("mongoose");

/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = require("body-parser");

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = require("cookie-parser");

/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = require("path");

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = require("webpack");

/***/ },
/* 8 */
/***/ function(module, exports) {

	module.exports = require("webpack-dev-middleware");

/***/ },
/* 9 */
/***/ function(module, exports) {

	module.exports = require("webpack-hot-middleware");

/***/ },
/* 10 */
/***/ function(module, exports) {

	module.exports = require("passport");

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _express = __webpack_require__(1);

	var _restaurant = __webpack_require__(12);

	var RestaurantController = _interopRequireWildcard(_restaurant);

	var _restaurant2 = __webpack_require__(21);

	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

	var router = new _express.Router();

	// Get all Restaurants
	router.route('/restaurants').get(_restaurant2.isAllowed, RestaurantController.getRestaurants).post(_restaurant2.isAllowed, RestaurantController.addRestaurant);

	// Get all Restaurant by name
	router.route('/restaurants/name/:name').get(_restaurant2.isAllowed, RestaurantController.getRestaurantByName);

	// Get Restaurant by Id
	router.route('/restaurants/id/:cuid').get(_restaurant2.isAllowed, RestaurantController.getRestaurantById);

	//Get all Restaurants by meals(dailyoffers)
	router.route('/restaurants/mealname/:mealname').get(_restaurant2.isAllowed, RestaurantController.getRestaurantsByMeal);

	//Get all Restaurants by restaurant type
	router.route('/restaurants/type/:type').get(_restaurant2.isAllowed, RestaurantController.getRestaurantsByType);

	//Get all Restaurants near cordinates
	router.route('/restaurants/location/:lat/:lng').get(_restaurant2.isAllowed, RestaurantController.getRestaurantsByCordinates);

	// Add a dailyOffer to Restaurant
	router.route('/restaurants/dailyoffers/:cuid').post(_restaurant2.isAllowed, RestaurantController.addDailyOfferToRestaurant);

	exports.default = router;

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.getRestaurantsByMeal = undefined;

	/**
	 * Get a single restaurant by meal
	 * @param req
	 * @param res
	 * @returns void
	 */
	var getRestaurantsByMeal = exports.getRestaurantsByMeal = function () {
	  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(req, res) {
	    var arrayMeals, arrayDailyOffers;
	    return regeneratorRuntime.wrap(function _callee$(_context) {
	      while (1) {
	        switch (_context.prev = _context.next) {
	          case 0:
	            _context.prev = 0;
	            _context.next = 3;
	            return _meal2.default.find({ name: req.params.mealname });

	          case 3:
	            arrayMeals = _context.sent;
	            _context.next = 6;
	            return _dailyoffer2.default.find({ meals: { $in: arrayMeals } });

	          case 6:
	            arrayDailyOffers = _context.sent;

	            _restaurant2.default.find({ dailyOffers: { $in: arrayDailyOffers } }).populate({ path: 'dailyOffers', populate: { path: 'meals' } }).exec(function (err, restaurants) {
	              if (err) {
	                res.status(500).send(err);
	              }
	              res.json({ restaurants: restaurants });
	            });
	            _context.next = 13;
	            break;

	          case 10:
	            _context.prev = 10;
	            _context.t0 = _context['catch'](0);

	            res.status(500).send(_context.t0);

	          case 13:
	          case 'end':
	            return _context.stop();
	        }
	      }
	    }, _callee, this, [[0, 10]]);
	  }));

	  return function getRestaurantsByMeal(_x, _x2) {
	    return _ref.apply(this, arguments);
	  };
	}();

	/**
	 * Get all restaurant near cordinates
	 * @param req
	 * @param res
	 * @returns void
	 */


	exports.getRestaurants = getRestaurants;
	exports.getRestaurantById = getRestaurantById;
	exports.getRestaurantByName = getRestaurantByName;
	exports.getRestaurantsByType = getRestaurantsByType;
	exports.getRestaurantsByCordinates = getRestaurantsByCordinates;
	exports.addRestaurant = addRestaurant;
	exports.addDailyOfferToRestaurant = addDailyOfferToRestaurant;

	var _restaurant = __webpack_require__(13);

	var _restaurant2 = _interopRequireDefault(_restaurant);

	var _dailyoffer = __webpack_require__(14);

	var _dailyoffer2 = _interopRequireDefault(_dailyoffer);

	var _meal = __webpack_require__(15);

	var _meal2 = _interopRequireDefault(_meal);

	var _user = __webpack_require__(16);

	var _user2 = _interopRequireDefault(_user);

	var _sanitizeHtml = __webpack_require__(19);

	var _sanitizeHtml2 = _interopRequireDefault(_sanitizeHtml);

	var _cuid = __webpack_require__(20);

	var _cuid2 = _interopRequireDefault(_cuid);

	var _mongoose = __webpack_require__(3);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

	function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

	/**
	 * Get all restaurants
	 * @param req
	 * @param res
	 * @returns void
	 */
	function getRestaurants(req, res) {

	  _restaurant2.default.find().exec(function (err, restaurants) {
	    if (err) {
	      res.status(500).send(err);
	    }
	    res.json({ restaurants: restaurants });
	  });
	}

	/**
	 * Get a single restaurant by Id
	 * @param req
	 * @param res
	 * @returns void
	 */
	function getRestaurantById(req, res) {
	  _restaurant2.default.findOne({ cuid: req.params.cuid }).populate({ path: 'dailyOffers', populate: { path: 'meals' } }).exec(function (err, restaurant) {
	    if (err) {
	      res.status(500).send(err);
	    }
	    res.json({ restaurant: restaurant });
	  });
	}

	/**
	 * Get a single restaurant by name
	 * @param req
	 * @param res
	 * @returns void
	 */
	function getRestaurantByName(req, res) {
	  _restaurant2.default.findOne({ name: req.params.name }).populate({ path: 'dailyOffers', populate: { path: 'meals' } }).exec(function (err, restaurant) {
	    if (err) {
	      res.status(500).send(err);
	    }
	    res.json({ restaurant: restaurant });
	  });
	}

	/**
	 * Get a single restaurant by name
	 * @param req
	 * @param res
	 * @returns void
	 */
	function getRestaurantsByType(req, res) {
	  _restaurant2.default.findOne({ restaurantType: req.params.type }).populate({ path: 'dailyOffers', populate: { path: 'meals' } }).exec(function (err, restaurant) {
	    if (err) {
	      res.status(500).send(err);
	    }
	    res.json({ restaurant: restaurant });
	  });
	}function getRestaurantsByCordinates(req, res) {
	  _restaurant2.default.find({ location: { $near: [req.params.lat, req.params.lng], $maxDistance: 1.4 } }).populate({ path: 'dailyOffers', populate: { path: 'meals' } }).exec(function (err, restaurant) {
	    if (err) {
	      res.status(500).send(err);
	    }
	    res.json({ restaurant: restaurant });
	  });
	}

	/**
	 * Save a restaurant
	 * @param req
	 * @param res
	 * @returns void
	 */
	function addRestaurant(req, res) {
	  if (!req.body.name || !req.body.lat || !req.body.lng || !req.body.restaurantType) {
	    res.status(403).end();
	  }

	  var newBody = Object.assign({}, req.body, { location: [Number(req.body.lat), Number(req.body.lng)] });

	  var newRestaurant = new _restaurant2.default(newBody);

	  // Let's sanitize inputs
	  newRestaurant.name = (0, _sanitizeHtml2.default)(newRestaurant.name);
	  newRestaurant.user = req.user._id;
	  newRestaurant.restaurantType = (0, _sanitizeHtml2.default)(newRestaurant.restaurantType);

	  newRestaurant.cuid = (0, _cuid2.default)();
	  newRestaurant.save(function (err, saved) {
	    if (err) {
	      res.status(500).send(err);
	    }
	    res.json({ newRestaurant: saved });
	  });
	}

	/**
	 * Add dailyOffer to restaurant
	 * @param req
	 * @param res
	 * @returns void
	 */
	function addDailyOfferToRestaurant(req, res) {
	  if (!req.params.cuid || !(req.body.dailyoffers.length > 0)) {
	    res.status(403).end();
	  }

	  _restaurant2.default.findOne({ cuid: req.params.cuid }).exec(function (err, restaurant) {
	    if (err) {
	      res.status(500).send(err);
	    }

	    restaurant.dailyOffers = [].concat(_toConsumableArray(JSON.parse(req.body.dailyoffers)));

	    restaurant.save(function (err, saved) {
	      if (err) {
	        res.status(500).send(err);
	      }
	      res.json({ restaurant: saved });
	    });
	  });
	}

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _mongoose = __webpack_require__(3);

	var _mongoose2 = _interopRequireDefault(_mongoose);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Schema = _mongoose2.default.Schema;

	var restaurantSchema = new Schema({
	  dailyOffers: [{ type: Schema.ObjectId, ref: 'DailyOffer' }],
	  name: { type: 'String', required: true, trim: true },
	  restaurantType: { type: 'String', required: true, trim: true },
	  location: { type: [Number], index: '2d', required: true },
	  user: { type: Schema.ObjectId, ref: 'User' },
	  cuid: { type: 'String', required: true }
	});

	exports.default = _mongoose2.default.model('Restaurant', restaurantSchema);

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _mongoose = __webpack_require__(3);

	var _mongoose2 = _interopRequireDefault(_mongoose);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Schema = _mongoose2.default.Schema;

	var dailyofferSchema = new Schema({
	  meals: [{ type: Schema.ObjectId, ref: 'Meal' }],
	  restaurantId: { type: Schema.ObjectId, ref: 'Restaurant' },
	  dateFrom: { type: Date, required: true },
	  dateTo: { type: Date, required: true },
	  cuid: { type: 'String', required: true }
	});

	exports.default = _mongoose2.default.model('DailyOffer', dailyofferSchema);

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _mongoose = __webpack_require__(3);

	var _mongoose2 = _interopRequireDefault(_mongoose);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Schema = _mongoose2.default.Schema;

	var mealSchema = new Schema({
	  name: { type: 'String', required: true, trim: true },
	  type: { type: 'String', required: true, trim: true },
	  description: { type: 'String', required: true, trim: true },
	  cuid: { type: 'String', required: true }
	});

	exports.default = _mongoose2.default.model('Meal', mealSchema);

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _mongoose = __webpack_require__(3);

	var _mongoose2 = _interopRequireDefault(_mongoose);

	var _crypto = __webpack_require__(17);

	var _crypto2 = _interopRequireDefault(_crypto);

	var _validator = __webpack_require__(18);

	var _validator2 = _interopRequireDefault(_validator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Schema = _mongoose2.default.Schema;

	var validateProperty = function validateProperty(property) {
	  return this.provider !== 'local' && !this.updated || property.length;
	};

	var validateEmail = function validateEmail(email) {
	  return this.provider !== 'local' && !this.updated || _validator2.default.isEmail(email, { require_tld: false });
	};

	var UserSchema = new Schema({
	  firstName: {
	    type: String,
	    trim: true,
	    default: '',
	    validate: [validateProperty, 'Please fill in your first name']
	  },
	  lastName: {
	    type: String,
	    trim: true,
	    default: '',
	    validate: [validateProperty, 'Please fill in your last name']
	  },
	  displayName: {
	    type: String,
	    trim: true
	  },
	  email: {
	    type: String,
	    index: {
	      unique: true,
	      sparse: true
	    },
	    lowercase: true,
	    trim: true,
	    default: '',
	    validate: [validateEmail, 'Please fill a valid email address']
	  },
	  username: {
	    type: String,
	    unique: 'Username already exists',
	    required: 'Please fill in a username',
	    lowercase: true,
	    trim: true
	  },
	  password: {
	    type: String,
	    default: ''
	  },
	  salt: {
	    type: String
	  },
	  provider: {
	    type: String,
	    required: 'Provider is required'
	  },
	  roles: {
	    type: [{
	      type: String,
	      enum: ['user', 'admin']
	    }],
	    default: ['user'],
	    required: 'Please provide at least one role'
	  },
	  updated: {
	    type: Date
	  },
	  created: {
	    type: Date,
	    default: Date.now
	  },
	  resetPasswordToken: {
	    type: String
	  },
	  resetPasswordExpires: {
	    type: Date
	  }
	});

	/**
	 * Hook a pre save method to hash the password
	 */
	UserSchema.pre('save', function (next) {
	  if (this.password && this.isModified('password')) {
	    this.salt = _crypto2.default.randomBytes(16).toString('base64');
	    this.password = this.hashPassword(this.password);
	  }
	  next();
	});

	/**
	 * Create instance method for hashing a password
	 */
	UserSchema.methods.hashPassword = function (password) {
	  if (this.salt && password) {
	    return _crypto2.default.pbkdf2Sync(password, new Buffer(this.salt, 'base64'), 10000, 64, 'SHA1').toString('base64');
	  } else {
	    return password;
	  }
	};

	/**
	 * Create instance method for authenticating user
	 */
	UserSchema.methods.authenticate = function (password) {
	  return this.password === this.hashPassword(password);
	};

	/**
	 * Find possible not used username
	 */
	UserSchema.statics.findUniqueUsername = function (username, suffix, callback) {
	  var _this = this;
	  var possibleUsername = username.toLowerCase() + (suffix || '');

	  _this.findOne({
	    username: possibleUsername
	  }, function (err, user) {
	    if (!err) {
	      if (!user) {
	        callback(possibleUsername);
	      } else {
	        return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
	      }
	    } else {
	      callback(null);
	    }
	  });
	};

	_mongoose2.default.model('User', UserSchema);

/***/ },
/* 17 */
/***/ function(module, exports) {

	module.exports = require("crypto");

/***/ },
/* 18 */
/***/ function(module, exports) {

	module.exports = require("validator");

/***/ },
/* 19 */
/***/ function(module, exports) {

	module.exports = require("sanitize-html");

/***/ },
/* 20 */
/***/ function(module, exports) {

	module.exports = require("cuid");

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.invokeRolesPolicies = invokeRolesPolicies;
	exports.isAllowed = isAllowed;

	var _acl = __webpack_require__(22);

	var _acl2 = _interopRequireDefault(_acl);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var aclInstance = new _acl2.default(new _acl2.default.memoryBackend());

	function invokeRolesPolicies() {
	  console.log('invoked roles policies...');
	  aclInstance.allow([{
	    roles: ['user'],
	    allows: [{
	      resources: '/restaurants',
	      permissions: '*'
	    }, {
	      resources: '/restaurants/name/:name',
	      permissions: '*'
	    }, {
	      resources: '/restaurants/id/:cuid',
	      permissions: '*'
	    }, {
	      resources: '/restaurants/mealname/:mealname',
	      permissions: '*'
	    }, {
	      resources: '/restaurants/location/:lat/:lng',
	      permissions: '*'
	    }, {
	      resources: '/restaurants/dailymeals/:cuid',
	      permissions: '*'
	    }]
	  }, {
	    roles: ['guest'],
	    allows: [{
	      resources: '/restaurants',
	      permissions: 'get'
	    }, {
	      resources: '/restaurants/name/:name',
	      permissions: 'get'
	    }, {
	      resources: '/restaurants/id/:cuid',
	      permissions: 'get'
	    }, {
	      resources: '/restaurants/mealname/:mealname',
	      permissions: 'get'
	    }, {
	      resources: '/restaurants/location/:lat/:lng',
	      permissions: 'get'
	    }]
	  }]);
	}

	function isAllowed(req, res, next) {
	  var roles = req.user ? req.user.roles : ['guest'];

	  aclInstance.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
	    if (err) {
	      return res.status(500).send('Unknown authorization error');
	    } else {
	      console.log('roles: ', roles);
	      if (isAllowed) {
	        console.log('allowed...');
	        return next();
	      } else {
	        return res.status(403).json({
	          message: 'User is not authorized!'
	        });
	      }
	    }
	  });
	}

/***/ },
/* 22 */
/***/ function(module, exports) {

	module.exports = require("acl");

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _express = __webpack_require__(1);

	var _user = __webpack_require__(24);

	var UserController = _interopRequireWildcard(_user);

	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

	var router = new _express.Router();

	// Authentication routes
	router.route('/signin').post(UserController.signin);
	router.route('/signup').post(UserController.signup);
	router.route('/signout').get(UserController.signout);

	exports.default = router;

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.signup = signup;
	exports.signin = signin;
	exports.signout = signout;
	exports.userByID = userByID;

	var _mongoose = __webpack_require__(3);

	var _mongoose2 = _interopRequireDefault(_mongoose);

	var _passport = __webpack_require__(10);

	var _passport2 = _interopRequireDefault(_passport);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var User = _mongoose2.default.model('User');

	/**
	 * Signup
	 */
	function signup(req, res) {
	  // For security measurement we remove the roles from the req.body object
	  delete req.body.roles;

	  // Init user and add missing fields
	  var user = new User(req.body);
	  user.provider = 'local';
	  user.displayName = user.firstName + ' ' + user.lastName;

	  console.log('here', user, User);

	  // Then save the user
	  user.save(function (err) {
	    if (err) {
	      return res.status(422).send({ message: err });
	    } else {
	      // Remove sensitive data before login
	      user.password = undefined;
	      user.salt = undefined;

	      req.login(user, function (err) {
	        if (err) {
	          res.status(400).send(err);
	        } else {
	          res.json(user);
	        }
	      });
	    }
	  });
	};

	/**
	 * Signin after passport authentication
	 */
	function signin(req, res, next) {
	  console.log('req.session', req.session);
	  console.log('req.user', req.user);
	  _passport2.default.authenticate('local', function (err, user, info) {
	    if (err || !user) {
	      res.clearCookie('sessionId').status(422).send(info);
	    } else {
	      // Remove sensitive data before login
	      user.password = undefined;
	      user.salt = undefined;
	      console.log('passed signin...');
	      req.logIn(user, function (err) {
	        console.log('logging in user', req.user);
	        if (err) {
	          res.status(400).send(err);
	        } else {
	          res.json(user);
	        }
	      });
	    }
	  })(req, res, next);
	};

	/**
	 * Signout
	 */
	function signout(req, res) {
	  req.logout();
	  res.redirect('/');
	};

	function userByID(req, res, next, id) {
	  if (!_mongoose2.default.Types.ObjectId.isValid(id)) {
	    return res.status(400).send({
	      message: 'User is invalid'
	    });
	  }

	  User.findOne({
	    _id: id
	  }).exec(function (err, user) {
	    if (err) {
	      return next(err);
	    } else if (!user) {
	      return next(new Error('Failed to load User ' + id));
	    }

	    req.profile = user;
	    next();
	  });
	};

/***/ },
/* 25 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  app: {
	    title: 'DailyMeals',
	    description: 'Find Your Best Lunch Nearby'
	  },
	  db: {
	    promise: global.Promise
	  },
	  port: process.env.PORT || 8000,
	  host: process.env.HOST || '0.0.0.0',
	  domain: process.env.DOMAIN,
	  mongoURL: process.env.MONGO_URL || 'mongodb://localhost:27017/dailymeal',
	  sessionCookie: {
	    maxAge: 24 * (60 * 60 * 1000),
	    httpOnly: false,
	    secure: false
	  },
	  sessionSecret: process.env.SESSION_SECRET || 'DailyMeals12345',
	  sessionKey: 'sessionId',
	  sessionCollection: 'sessions',
	  csrf: {
	    csrf: false,
	    csp: false,
	    xframe: 'SAMEORIGIN',
	    p3p: 'ABCDEF',
	    xssProtection: true
	  },
	  illegalUsernames: ['meanjs', 'administrator', 'password', 'admin', 'user', 'unknown', 'anonymous', 'null', 'undefined', 'api']
	};

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = initSession;

	var _lusca = __webpack_require__(27);

	var _lusca2 = _interopRequireDefault(_lusca);

	var _expressSession = __webpack_require__(28);

	var _expressSession2 = _interopRequireDefault(_expressSession);

	var _connectMongo = __webpack_require__(29);

	var _connectMongo2 = _interopRequireDefault(_connectMongo);

	var _mongoose = __webpack_require__(3);

	var _mongoose2 = _interopRequireDefault(_mongoose);

	var _config = __webpack_require__(25);

	var _config2 = _interopRequireDefault(_config);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var MongoStore = (0, _connectMongo2.default)(_expressSession2.default);

	function initSession(app) {
	  console.log('session...');
	  app.use((0, _expressSession2.default)({
	    saveUninitialized: true,
	    resave: true,
	    secret: _config2.default.sessionSecret,
	    cookie: {
	      maxAge: _config2.default.sessionCookie.maxAge,
	      httpOnly: _config2.default.sessionCookie.httpOnly,
	      secure: _config2.default.sessionCookie.secure && _config2.default.secure.ssl
	    },
	    name: _config2.default.sessionKey,
	    store: new MongoStore({
	      mongooseConnection: _mongoose2.default.connection,
	      collection: _config2.default.sessionCollection
	    })
	  }));

	  app.use((0, _lusca2.default)(_config2.default.csrf));
	};

/***/ },
/* 27 */
/***/ function(module, exports) {

	module.exports = require("lusca");

/***/ },
/* 28 */
/***/ function(module, exports) {

	module.exports = require("express-session");

/***/ },
/* 29 */
/***/ function(module, exports) {

	module.exports = require("connect-mongo");

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _mongoose = __webpack_require__(3);

	var _mongoose2 = _interopRequireDefault(_mongoose);

	var _localStrategy = __webpack_require__(31);

	var _localStrategy2 = _interopRequireDefault(_localStrategy);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var User = _mongoose2.default.model('User');

	var initPassport = function initPassport(app, passport) {

	  passport.serializeUser(function (user, done) {
	    console.log('serialize user...');
	    done(null, user.id);
	  });

	  passport.deserializeUser(function (id, done) {
	    console.log('deserialize user...');
	    User.findOne({
	      _id: id
	    }, '-salt -password', function (err, user) {
	      done(err, user);
	    });
	  });

	  (0, _localStrategy2.default)();
	};

	module.exports = initPassport;

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _passport = __webpack_require__(10);

	var _passport2 = _interopRequireDefault(_passport);

	var _passportLocal = __webpack_require__(32);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var User = __webpack_require__(3).model('User');
	/**
	 * Module dependencies
	 */

	var PassportStrategy = function PassportStrategy() {
	  // Use local strategy
	  _passport2.default.use(new _passportLocal.Strategy({
	    usernameField: 'username',
	    passwordField: 'password'
	  }, function (username, password, done) {
	    User.findOne({
	      $or: [{
	        username: username.toLowerCase()
	      }, {
	        email: username.toLowerCase()
	      }]
	    }, function (err, user) {
	      if (err) {
	        return done(err);
	      }
	      if (!user || !user.authenticate(password)) {
	        return done(null, false, {
	          message: 'Invalid username or password (' + new Date().toLocaleTimeString() + ')'
	        });
	      }
	      return done(null, user);
	    });
	  }));
	};

	exports.default = PassportStrategy;

/***/ },
/* 32 */
/***/ function(module, exports) {

	module.exports = require("passport-local");

/***/ }
/******/ ]);