import lusca from 'lusca'
import session from 'express-session'
import connectMongo from 'connect-mongo'
import mongoose from 'mongoose'

import config from './config'

const MongoStore = connectMongo(session)

export default function initSession(app) {
  console.log('session...')
  app.use(session({
    saveUninitialized: true,
    resave: true,
    secret: config.sessionSecret,
    cookie: {
      maxAge: config.sessionCookie.maxAge,
      httpOnly: config.sessionCookie.httpOnly,
      secure: config.sessionCookie.secure && config.secure.ssl
    },
    name: config.sessionKey,
    store: new MongoStore({
      mongooseConnection: mongoose.connection,
      collection: config.sessionCollection
    })
  }));

  app.use(lusca(config.csrf));
};
