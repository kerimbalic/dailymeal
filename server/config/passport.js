import mongoose from 'mongoose'
import localStrategy from './strategies/local-strategy'

const User = mongoose.model('User')

const initPassport = function(app, passport) {

  passport.serializeUser(function (user, done) {
    console.log('serialize user...')
    done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    console.log('deserialize user...')
    User.findOne({
      _id: id
    }, '-salt -password', function (err, user) {
      done(err, user);
    });
  });

  localStrategy();
};

module.exports = initPassport
