export default {
  app: {
    title: 'DailyMeals',
    description: 'Find Your Best Lunch Nearby'
  },
  db: {
    promise: global.Promise
  },
  port: process.env.PORT || 8000,
  host: process.env.HOST || '0.0.0.0',
  domain: process.env.DOMAIN,
  mongoURL: process.env.MONGO_URL || 'mongodb://localhost:27017/dailymeal',
  sessionCookie: {
    maxAge: 24 * (60 * 60 * 1000),
    httpOnly: false,
    secure: false
  },
  sessionSecret: process.env.SESSION_SECRET || 'DailyMeals12345',
  sessionKey: 'sessionId',
  sessionCollection: 'sessions',
  csrf: {
    csrf: false,
    csp: false,
    xframe: 'SAMEORIGIN',
    p3p: 'ABCDEF',
    xssProtection: true
  },
  illegalUsernames: ['meanjs', 'administrator', 'password', 'admin', 'user',
    'unknown', 'anonymous', 'null', 'undefined', 'api'
  ]
};
