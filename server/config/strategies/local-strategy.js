import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'

const User = require('mongoose').model('User');
/**
 * Module dependencies
 */

const PassportStrategy = function () {
  // Use local strategy
  passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  },
  function (username, password, done) {
    User.findOne({
      $or: [{
        username: username.toLowerCase()
      }, {
        email: username.toLowerCase()
      }]
    }, function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user || !user.authenticate(password)) {
        return done(null, false, {
          message: 'Invalid username or password (' + (new Date()).toLocaleTimeString() + ')'
        });
      }
      return done(null, user);
    });
  }));
};

export default PassportStrategy
