import { Router } from 'express';
import * as RestaurantController from '../controllers/restaurant.controller';
import * as MealController from '../controllers/meal.controller';
import * as DailyOfferController from '../controllers/dailyoffer.controller';
import { isAllowed } from '../policies/dailymeal.policies'

const router = new Router();


// Get all Restaurants
router.route('/restaurants')
  .get(isAllowed, RestaurantController.getRestaurants)
  .post(isAllowed, RestaurantController.addRestaurant);

// Get all Restaurant by name
router.route('/restaurants/name/:name').get(isAllowed, RestaurantController.getRestaurantByName);

// Get Restaurant by Id
router.route('/restaurants/id/:cuid').get(isAllowed, RestaurantController.getRestaurantById);

//Get all Restaurants by meals(dailyoffers)
router.route('/restaurants/mealname/:mealname').get(isAllowed, RestaurantController.getRestaurantsByMeal);

//Get all Restaurants by restaurant type
router.route('/restaurants/type/:type').get(isAllowed, RestaurantController.getRestaurantsByType);

//Get all Restaurants near cordinates
router.route('/restaurants/location/:lat/:lng').get(isAllowed, RestaurantController.getRestaurantsByCordinates);

// Add a dailyOffer to Restaurant
router.route('/restaurants/dailyoffers/:cuid').post(isAllowed, RestaurantController.addDailyOfferToRestaurant);

// Add new Meal
router.route('/meals').post(isAllowed, MealController.addMeal);

// Add new DailyOffer
router.route('/dailyoffers').post(isAllowed, DailyOfferController.addDailyOffer);

export default router;
