import { Router } from 'express';
import * as MealController from '../controllers/meal.controller';
import { isAllowed } from '../policies/restaurant.policies'

const router = new Router();


// Add new Meal
router.route('/meals').post(isAllowed, MealController.addMeal);


export default router;
