import { Router } from 'express';
import * as UserController from '../controllers/user.controller';

const router = new Router();

// Authentication routes
router.route('/signin').post(UserController.signin);
router.route('/signup').post(UserController.signup);
router.route('/signout').get(UserController.signout);

export default router;
