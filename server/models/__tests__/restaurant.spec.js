import test from 'ava';
import request from 'supertest';
import app from '../../server';
import Restaurant from '../restaurant';
import { connectDB, dropDB } from '../../util/test-helpers';

// Initial posts added into test db
const restaurants = [
  new Restaurant({ "_id" : ObjectId("587b90b18b90bbc0be8d1718"), "dailyOffers" : null, "name" : "Lovac", "user" : null, "cuid" : "fsgsfdsfdf123", "location" : [ 43.84864, 18.35644 ] }),
  new Restaurant({ "_id" : ObjectId("587b9e758b90bbc0be8d171a"), "dailyOffers" : null, "name" : "Basari","user" : null, "cuid" : "fgfsgsfdsfdf345", "location" : [ 43.95128, 18.27128 ] }),
  new Restaurant({ "_id" : ObjectId("587bb2038b90bbc0be8d171b"), "dailyOffers" : null, "name" : "Kod Kibeta","user" : null, "cuid" : "fgdfdfsgsfdsfdf","location" : [ 41.95128, 17.27128 ]})
];

test.beforeEach('connect and add two inital restaurants entries', t => {
  connectDB(t, () => {
    Restaurant.create(restaurants, err => {
      if (err) t.fail('Unable to create restaurants');
    });
  });
});

test.afterEach.always(t => {
  dropDB(t);
});

test.serial('Should correctly give number of restaurants', async t => {
  t.plan(2);

  const res = await request(app)
    .get('/api/restaurants')
    .set('Accept', 'application/json');

  t.is(res.status, 200);
  t.deepEqual(restaurants.length, res.body.restaurants.length);
});

test.serial('Should send correct data when queried against a name', async t => {
  t.plan(2);

  const restaurant =   new Restaurant({ "_id" : ObjectId("587b90b18b90bbc0be8d1718"), "dailyOffers" : [ ObjectId("587b8fdf95a249b19328af3b") ], "name" : "Lovac", "user" : null, "cuid" : "fsgsfdsfdf", "location" : [ 43.84864, 18.35644 ] });
  restaurant.save();

  const res = await request(app)
    .get('/api/restaurants/Lovac')
    .set('Accept', 'application/json');

  t.is(res.status, 200);
  t.is(res.body.restaurant.name, restaurant.name);
});

