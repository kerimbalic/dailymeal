import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const mealSchema = new Schema({
  name: { type: 'String', required: true, trim: true},
  type: { type: 'String', required: true, trim: true},
  description: { type: 'String', required: true, trim: true},
  cuid: {type: 'String', required:true },
});

export default mongoose.model('Meal', mealSchema);