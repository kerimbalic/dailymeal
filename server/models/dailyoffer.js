import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const dailyofferSchema = new Schema({
  meals: [{ type: Schema.ObjectId, ref: 'Meal' }],
  restaurantId: { type: Schema.ObjectId, ref: 'Restaurant'  },
  dateFrom: { type: Date, required: true },
  dateTo: { type:Date, required:true },
  cuid: {type: 'String', required:true },
});

export default mongoose.model('DailyOffer', dailyofferSchema);
