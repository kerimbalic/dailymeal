import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const restaurantSchema = new Schema({
  dailyOffers: [{ type: Schema.ObjectId, ref: 'DailyOffer' }],
  name: { type: 'String', required: true, trim: true},
  restaurantType: { type: 'String', required: true, trim: true},
  location: { type: [Number], index: '2d', required: true },
  user: { type:Schema.ObjectId, ref: 'User' },
  cuid: {type: 'String', required:true },
});

export default mongoose.model('Restaurant', restaurantSchema);
