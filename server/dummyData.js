import Restaurant from '../models/restaurant';
import DailyOffer from '../models/dailyoffer';
import Meal from '../models/meal';
import User from '../models/user';
import sanitizeHtml from 'sanitize-html';
import cuid from 'cuid';

export default function () {
  Post.count().exec((err, count) => {
    if (count > 0) {
      return;
    }

    const meal1 = new Meal({"name" : "Pizza", "type" : "starter", "description" : "Slice of pizza"})
    const meal2 = new Meal({"name" : "Steak", "type" : "main", "description" : "Beef meat" })

    const dailyoffer1 = new DailyOffer("meals" : [ ObjectId("587b8ee541b2387b756a189f"), ObjectId("587b8f0441b2387b756a18a0") ], "restaurantId" : null, "dateFrom" : null, "dateTo" : null, "cuid" : cuid())
    const dailyoffer2 = new DailyOffer()


    const post1 = new Post({ name: 'Admin', title: 'Hello MERN', slug: 'hello-mern', cuid: 'cikqgkv4q01ck7453ualdn3hd', content: content1 });
    const post2 = new Post({ name: 'Admin', title: 'Lorem Ipsum', slug: 'lorem-ipsum', cuid: 'cikqgkv4q01ck7453ualdn3hf', content: content2 });

    Post.create([post1, post2], (error) => {
      if (!error) {
        // console.log('ready to go....');
      }
    });
  });
}
