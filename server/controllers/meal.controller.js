import Meal from '../models/meal';
import User from '../models/user';
import sanitizeHtml from 'sanitize-html';
import cuid from 'cuid';
import { Schema } from 'mongoose';


/**
 * Save a meal
 * @param req
 * @param res
 * @returns void
 */
export function addMeal(req, res) {
  /* if (!req.body.name || !req.body.type || !req.body.description) {
  	console.log("ussoooo")
    res.status(403).end();
  } */

  const newMeal = new Meal(req.body);

  // Let's sanitize inputs
  newMeal.name = sanitizeHtml(newMeal.name);
  newMeal.type = sanitizeHtml(newMeal.type);
  newMeal.description = sanitizeHtml(newMeal.description);
  newMeal.cuid = cuid();

  newMeal.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.json({ newMeal: saved });
    }
  });
}