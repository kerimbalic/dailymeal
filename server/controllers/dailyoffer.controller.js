import DailyOffer from '../models/dailyoffer';
import Meal from '../models/meal';
import User from '../models/user';
import sanitizeHtml from 'sanitize-html';
import cuid from 'cuid';
import { Schema } from 'mongoose';


/**
 * Save a restaurant
 * @param req
 * @param res
 * @returns void
 */
export function addDailyOffer(req, res) {
  if (!req.body.dateFrom || !req.body.dateTo || !(req.body.meals.length>0)) {
    res.status(403).end();
  }

  const newDailyOffer = new DailyOffer(req.body);

  // Let's sanitize inputs

  newDailyOffer.meals= [...JSON.parse(req.body.meals)];

  newDailyOffer.cuid = cuid();

  newDailyOffer.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    else{
    res.json({ newDailyOffer: saved });
    }
  });
}

