import mongoose from 'mongoose';
import passport from 'passport';

const User = mongoose.model('User');

/**
 * Signup
 */
export function signup(req, res) {
  // For security measurement we remove the roles from the req.body object
  delete req.body.roles;

  // Init user and add missing fields
  var user = new User(req.body);
  user.provider = 'local';
  user.displayName = user.firstName + ' ' + user.lastName;

  console.log('here', user, User)

  // Then save the user
  user.save((err) => {
    if (err) {
      return res.status(422).send({ message: err});
    } else {
      // Remove sensitive data before login
      user.password = undefined;
      user.salt = undefined;

      req.login(user, function (err) {
        if (err) {
          res.status(400).send(err);
        } else {
          res.json(user);
        }
      });
    }
  });
};

/**
 * Signin after passport authentication
 */
export function signin(req, res, next) {
  console.log('req.session', req.session)
  console.log('req.user', req.user)
  passport.authenticate('local', (err, user, info) => {
    if (err || !user) {
      res.clearCookie('sessionId').status(422).send(info);
    } else {
      // Remove sensitive data before login
      user.password = undefined;
      user.salt = undefined;
      console.log('passed signin...')
      req.logIn(user, (err) => {
        console.log('logging in user', req.user)
        if (err) {
          res.status(400).send(err);
        } else {
          res.json(user);
        }
      });
    }
  })(req, res, next);
};

/**
 * Signout
 */
export function signout(req, res) {
  req.logout();
  res.redirect('/');
};

export function userByID(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'User is invalid'
    });
  }

  User.findOne({
    _id: id
  }).exec(function (err, user) {
    if (err) {
      return next(err);
    } else if (!user) {
      return next(new Error('Failed to load User ' + id));
    }

    req.profile = user;
    next();
  });
};
