import Restaurant from '../models/restaurant';
import DailyOffer from '../models/dailyoffer';
import Meal from '../models/meal';
import User from '../models/user';
import sanitizeHtml from 'sanitize-html';
import cuid from 'cuid';
import { Schema } from 'mongoose';

/**
 * Get all restaurants
 * @param req
 * @param res
 * @returns void
 */
export function getRestaurants(req, res) {

  Restaurant.find().exec((err, restaurants) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ restaurants });
  });
}


/**
 * Get a single restaurant by Id
 * @param req
 * @param res
 * @returns void
 */
export function getRestaurantById(req, res) {
  Restaurant.findOne({ cuid: req.params.cuid})
    .populate({path: 'dailyOffers', populate: {path: 'meals'}})
    .exec((err, restaurant) => {
      if (err) {
        res.status(500).send(err);
      }
      res.json({ restaurant });
    });
}

/**
 * Get a single restaurant by name
 * @param req
 * @param res
 * @returns void
 */
export function getRestaurantByName(req, res) {
  Restaurant.findOne({ name: req.params.name })
    .populate({path: 'dailyOffers', populate: {path: 'meals'}})
    .exec((err, restaurant) => {
      if (err) {
        res.status(500).send(err);
      }
      res.json({ restaurant });
    });
}

/**
 * Get a single restaurant by name
 * @param req
 * @param res
 * @returns void
 */
export function getRestaurantsByType(req, res) {
  Restaurant.findOne({ restaurantType: req.params.type })
    .populate({path: 'dailyOffers', populate: {path: 'meals'}})
    .exec((err, restaurant) => {
      if (err) {
        res.status(500).send(err);
      }
      res.json({ restaurant });
    });
}

/**
 * Get a single restaurant by meal
 * @param req
 * @param res
 * @returns void
 */
export async function getRestaurantsByMeal(req, res) {
  try{
		const arrayMeals = await Meal.find({name:req.params.mealname})
		const arrayDailyOffers = await DailyOffer.find({meals: {$in: arrayMeals}})
 		Restaurant.find({ dailyOffers: {$in: arrayDailyOffers}})
      .populate({path: 'dailyOffers', populate: {path: 'meals'}})
      .exec((err, restaurants) => {
      	if (err) {
        	res.status(500).send(err);
      	}
      	res.json({ restaurants });
    		});
	}
	catch(err)
	{
		res.status(500).send(err);
	}
}

/**
 * Get all restaurant near cordinates
 * @param req
 * @param res
 * @returns void
 */
export function getRestaurantsByCordinates(req, res) {
  Restaurant.find({location: {$near: [req.params.lat,req.params.lng], $maxDistance: 1.4 }})
  .populate({path: 'dailyOffers', populate: {path: 'meals'}})
  .exec((err, restaurant) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ restaurant });
  });
}

/**
 * Save a restaurant
 * @param req
 * @param res
 * @returns void
 */
export function addRestaurant(req, res) {
  if (!req.body.name || !req.body.lat || !req.body.lng || !req.body.restaurantType) {
    res.status(403).end();
  }

  const newBody = Object.assign({}, req.body, {location: [Number(req.body.lat), Number(req.body.lng)]});

  const newRestaurant = new Restaurant(newBody);

  // Let's sanitize inputs
  newRestaurant.name = sanitizeHtml(newRestaurant.name);
  newRestaurant.user = req.user._id;
  newRestaurant.restaurantType = sanitizeHtml(newRestaurant.restaurantType);

  newRestaurant.cuid = cuid();
  newRestaurant.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ newRestaurant: saved });
  });
}


/**
 * Add dailyOffer to restaurant
 * @param req
 * @param res
 * @returns void
 */
export function addDailyOfferToRestaurant(req, res) {
  if (!req.params.cuid || !(req.body.dailyoffers.length>0)) {
    res.status(403).end();
  }

  Restaurant.findOne({ cuid: req.params.cuid}).exec((err, restaurant) => {
    if (err) {
      res.status(500).send(err);
    }

    restaurant.dailyOffers= [...JSON.parse(req.body.dailyoffers)];

    restaurant.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ restaurant: saved });
    });

  });
}
