import Express from 'express';
import compression from 'compression';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import path from 'path';


// Webpack Requirements
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import passport from 'passport'

// Initialize the Express App
const app = new Express();

// Import required modules
import dailymeal from './routes/dailymeal.routes'
import users from './routes/user.routes'

import serverConfig from './config/config';
// import initPassport from './config/passport';
import initSession from './config/session';

import { invokeRolesPolicies as dailymealPolicies } from './policies/dailymeal.policies';


// Set native promises as mongoose promise
mongoose.Promise = global.Promise;

app.use(Express.static(path.resolve(__dirname, '../dist')));
app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: true }));
app.use(compression());
app.use(cookieParser());
initSession(app)
app.use(passport.initialize());
app.use(passport.session());

// MongoDB Connection
mongoose.connect(serverConfig.mongoURL, (error) => {
  if (error) {
    console.error('Please make sure Mongodb is installed and running!'); // eslint-disable-line no-console
    throw error;
  } else {
    const db = mongoose.connection
    const initPassport = require('./config/passport')
    initPassport(app, passport)
    dailymealPolicies()
  }
});

app.use('/api', dailymeal);
app.use('/users', users);

app.use((req, res, next) => res.status(200).send('Not found!'));

// start app
app.listen(serverConfig.port, (error) => {
  if (!error) {
    console.log(`Daily Meal Server is running on: ${serverConfig.port}!`); // eslint-disable-line
  }
});

export default app;
