import acl from 'acl'

const aclInstance = new acl(new acl.memoryBackend())

export function invokeRolesPolicies () {
  console.log('invoked roles policies...')
  aclInstance.allow([{
      roles: ['user'],
      allows: [{
        resources: '/restaurants',
        permissions: '*'
      }, {
        resources: '/restaurants/name/:name',
        permissions: '*'
      }, {
        resources: '/restaurants/id/:cuid',
        permissions: '*'
      }, {
        resources: '/restaurants/mealname/:mealname',
        permissions: '*'
      }, {
        resources: '/restaurants/location/:lat/:lng',
        permissions: '*'
      }, {
        resources: '/restaurants/dailymeals/:cuid',
        permissions: '*'
      },{
        resources: '/meals',
        permissions: '*'
      },{
        resources: '/dailyoffers',
        permissions: '*'
      }]
    }, {
      roles: ['guest'],
      allows: [{
        resources: '/restaurants',
        permissions: 'get'
      }, {
        resources: '/restaurants/name/:name',
        permissions: 'get'
      }, {
        resources: '/restaurants/id/:cuid',
        permissions: 'get'
      }, {
        resources: '/restaurants/mealname/:mealname',
        permissions: 'get'
      }, {
        resources: '/restaurants/location/:lat/:lng',
        permissions: 'get'
      }]
    }
  ])
}

export function isAllowed (req, res, next) {
  const roles = (req.user) ? req.user.roles : ['guest']

  aclInstance.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, isAllowed) => {
    if (err) {
      return res.status(500).send('Unknown authorization error')
    } else {
      console.log('roles: ', roles)
      if (isAllowed) {
        console.log('allowed...')
        return next()
      } else {
        return res.status(403).json({
          message: 'User is not authorized!'
        })
      }
    }
  })
}
